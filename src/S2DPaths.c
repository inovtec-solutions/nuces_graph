/*
 * S2DPaths.cpp
 *
 *  Created on: Nov 2, 2018
 *      Author: baharali
 */

#include <string.h>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "nucesGraph.h"

/* Find if Vertex exist in a Graph; return 1 if exist else 0 */
int vertexExists(struct nGraph *G, int vertex)
{
	struct nGraph vertices = getVertices(G);
	int vertexFlage = 0;
    while(vertices.V->head!=NULL)
    {
    		if (vertex == vertices.V->head->label)
    		{
    			vertexFlage = 1;
    			break;
    		}
			vertices.V->head = vertices.V->head->next;
    }

    return vertexFlage;
}

/* Print Path from Graph */
void printPath(struct nGraph *G)
{
	struct nGraph vertices  = getVertices(G);
	while(vertices.V->head!=NULL)
	{
		printf("%d", vertices.V->head->label);
		vertices.V->head = vertices.V->head->next;
		if (vertices.V->head!=NULL)
			printf("->");
	 }
	printf("\n");
}

/* Calculate all Paths from Source Vertex to Destination Vertex*/
void getAllPaths(int source_vertex, int destination_vertex, struct nGraph *mainGraph, struct nGraph *pathGraph)
{
	/* Print Once, when method is called for first time*/
	if (pathGraph->V->head == NULL)
		printf("\nPaths (%d to %d): \n", source_vertex, destination_vertex);

	/* Find if Source Vertex is in Path Graph*/
	int vertexFlage = vertexExists(pathGraph, source_vertex);

	/* Check if Destination Vertex is reached*/
	if (source_vertex == destination_vertex)
    {
    	addVertex(pathGraph, source_vertex);
    	printPath(pathGraph);
    }
    else
    {
    	/* If Source Vertex not in Path Graph, add it to Path Graph*/
    	if (vertexFlage == 0)
    		addVertex(pathGraph, source_vertex);

    	/* Get Adjacent Vertexs*/
    	struct nGraph neighbours = getNeighbours(mainGraph, source_vertex);
		while(neighbours.V->head!=NULL)
		{
			vertexFlage = vertexExists(pathGraph, neighbours.V->head->label);
			if (vertexFlage == 0)
			{
				struct nGraph newPathGraph = getVertices(pathGraph);
				getAllPaths(neighbours.V->head->label, destination_vertex, mainGraph, &newPathGraph);
			}
			neighbours.V->head = neighbours.V->head->next;
		}
    }

}
