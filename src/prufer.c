/*
 * S2DPaths.cpp
 *
 *  Created on: Nov 2, 2018
 *      Author: baharali
 */

#include <string.h>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "nucesGraph.h"


int getRandomNumber(int MaxRange)
{
	return rand() % MaxRange;
}

struct prufer getRandomPruferCode(int pruferSize, int pruferMaxNumber)
{
	struct prufer P = newPrufer("Prufer");
	for (int i = 0; i < pruferSize; i++)
	{
		int vertex = getRandomNumber(pruferMaxNumber);
		addVertexPrufer(&P, vertex);
	}

	return P;
}

struct nGraph getRandomPruferTree(int pruferSize, int pruferMaxNumber)
{
	struct prufer RP = getRandomPruferCode(pruferSize, pruferMaxNumber);
	showPrufer(&RP);

	struct nGraph V = newGraph("G");
	struct vertex *tmp = RP.V->head;

	while(tmp != NULL)
	{
		addVertex(&V, tmp->label);
		tmp = tmp->next;
	}

	while(V.V->count < (RP.V->count + 2))
	{
		int vertex = getRandomNumber(pruferMaxNumber);
		addVertex(&V, vertex);
	}

	show(&V);
	struct nGraph RPT = generateTreeFromPruferCode(&RP, &V);
	return RPT;
}

void showPrufer(struct prufer *P)
{
	printf("\nPrufer Code: {");
	struct vertex *tmp = P->V->head;
	while(tmp != NULL)
	{
		if(tmp->next == NULL)
			printf("%d", tmp->label);
		else
			printf("%d, ", tmp->label);

		tmp = tmp->next;
	}
	printf("}\n");
}

void copyVerticesPrufer(struct prufer *src, struct prufer *dest)
{
	struct vertex *tmp = src->V->head;
	while(tmp != NULL) {
		addVertexPrufer(dest, tmp->label);
		tmp = tmp->next;
	}
}

struct prufer getVerticesPrufer(struct prufer *src)
{
	struct prufer result;
	pruferInit(&result, "P");
	copyVerticesPrufer(src, &result);
	return result;
}

int getPendantVertex(struct nGraph *G)
{
	updateDegrees(G);
	struct vertex *tmp = G->V->head;
	while(tmp!=NULL)
    {
    	if (tmp->degree == 1)
    	{
    		int vertexLabel = tmp->label;
    		return vertexLabel;
    	}
    	tmp = tmp->next;
    }
    return -1;
}

void sortVertices(struct nGraph *G)
{
	struct vertex *tmp = G->V->head;
	struct vertex swap;

	while(tmp != NULL)
	{
		if ( tmp->next != NULL)
		{
			struct vertex *tmp2 = tmp->next;
			while(tmp2 != NULL)
			{
				if (tmp->label > tmp2->label)
				{
					swap.label = tmp->label;
					tmp->label   = tmp2->label;
					tmp2->label = swap.label;
				}

				tmp2 = tmp2->next;
			}
		}
		tmp = tmp->next;
	}
}
int findMinimumVertex(struct nGraph *G, struct prufer *P)
{
	struct vertex *tmp = G->V->head;
	int minVertex = tmp->label;
	while(tmp!=NULL)
    {
		int flag = 0;
		minVertex = tmp->label;

		struct vertex *tmp2 = P->V->head;
		while(tmp2 != NULL)
		{
			if(tmp2->label == tmp->label)
			{
				flag = 1;
				break;
			}
			tmp2 = tmp2->next;
		}

		if (flag == 0)
			return tmp->label;

		tmp = tmp->next;
    }
	return minVertex;
}

void addVertexPrufer(struct prufer *P, int c)
{
	struct vertex *tmp = malloc(sizeof(struct vertex));
	tmp->label = c;

	if (P->V->count == 0) {
		P->V->head = tmp;
		P->V->tail = tmp;
		P->V->count++;
	}
	else {
		P->V->tail->next = tmp;
		P->V->tail       = tmp;
		P->V->count++;
	}
}

void pruferInit(struct prufer *P, char *t)
{
	P->V = malloc(sizeof(struct verList));
	P->V->count = 0;
	P->label = malloc(sizeof(char)*10);
	strcpy(P->label, t);
}

struct prufer newPrufer(char *c)
{
	struct prufer result;
	pruferInit(&result, c);
	return result;
}

struct prufer generatePruferCodeFromTree(struct nGraph *V)
{
	//struct nGraph V = getVertices(G);
	struct prufer P = newPrufer("Prufer");
	int countVertices = V->V->count;
	int VS = -1;
	sortVertices(V);
	while (countVertices > 2)
	{
		VS = getPendantVertex(V);
		struct nGraph VN = getNeighbours(V, VS);
		int VNV = -1;
		if(VN.V->head!=NULL)
		{
			VNV = VN.V->head->label;
		}

		addVertexPrufer(&P, VNV);
		removeVertex(V, VS);
		updateDegrees(V);
		countVertices = V->V->count;
	}

	return P;
}

void removeVertexPrufer(struct prufer *P, int c)
{
	struct vertex *tmp = P->V->head;
	struct vertex *pre;

	if (P->V->head->label == c) {
		P->V->head = tmp->next;
		P->V->count--;
	}
	else {
		pre = tmp;
		tmp = tmp->next;
		while(tmp != NULL) {
			if (tmp->label == c) {
				pre->next = tmp->next;
				P->V->count--;
				break;
			}
			else {
				pre = tmp;
				tmp = tmp->next;
			}
		}
	}
}

struct nGraph generateTreeFromPruferCode(struct prufer *P, struct nGraph *Vertices)
{
	int countVertices = Vertices->V->count;
	struct nGraph G = newGraph("G");
	sortVertices(Vertices);

	while (countVertices > 2)
	{
		int Vi = findMinimumVertex(Vertices, P);
		int V0 = P->V->head->label;
		addVertex(&G, Vi);
		addVertex(&G, V0);
		addEdge(&G, Vi, V0, 0);
		removeVertex(Vertices, Vi);
		removeVertexPrufer(P, V0);
		countVertices = Vertices->V->count;
	}

	int V0 = Vertices->V->head->label;
	int V1 = Vertices->V->head->next->label;

	addVertex(&G, V0);
	addVertex(&G, V1);
	addEdge(&G, V0, V1, 0);

	return G;
}

void updateDegrees(struct nGraph *G)
{
	struct vertex *tmp = G->V->head;

	while(tmp !=NULL)
	{
		struct nGraph neighbour = getNeighbours(G, tmp->label);
		int degreeValue = neighbour.V->count;
		tmp->degree = degreeValue;
		tmp = tmp->next;
	}

}
