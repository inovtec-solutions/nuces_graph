/**
 * @file doxygen_c.h
 * @author My Self
 * @date 9 Sep 2012
 * @brief File containing example of doxygen usage for quick reference.
 *
 * Here typically goes a more extensive explanation of what the header
 * defines. Doxygens tags are words preceeded by either a backslash @\
 * or by an at symbol @@.
 * @see http://www.stack.nl/~dimitri/doxygen/docblocks.html
 * @see http://www.stack.nl/~dimitri/doxygen/commands.html
 */

#ifndef NUCESGRAPH_H
#define NUCESGRAPH_H


struct vertex
{
	int label;
	char *lblString;
	int color;
	int eccentricity;
	int degree;
	struct vertex *next;
	struct vertex *prev;
};

/**
 * @brief Use brief, otherwise the index won't have a brief explanation.
 *
 * Detailed explanation.
 */

struct edge 
{
	int weight;
	int head;
	int tail;
	struct edge *next;
	struct edge *prev;
};

struct verList
{
	int count;
	struct vertex *head;
	struct vertex *tail; 
};

struct edgList
{
	int count;
	struct edge *head;
	struct edge *tail;
};

struct nGraph
{
	char *label;
	struct verList *V;
	struct edgList *E;
};

struct prufer
{
	char *label;
	struct verList *V;
};

/** My function doing something...
    @param param1 first parameter
    @param param2 second parameter
    @return value return value
*/
struct nGraph newGraph(char *);
void nGraphInit(struct nGraph *, char *);
void addVertex(struct nGraph *, int);
void addVertexLabel(struct nGraph *, char *);
void addVertexDuplicateOK(struct nGraph *, int);
void show(struct nGraph *);
void addEdge(struct nGraph *, int, int, int);
void addEdgeLabel(struct nGraph *, char *, char *, int);
void showDot(struct nGraph *);
void exportDot(struct nGraph *);
int getVertexColor(struct nGraph *, int);
int searchVertex(struct nGraph *, int);
int searchVertexLabel(struct nGraph *, char *);
int eccentricity(struct nGraph *, int);
int edgeExists(struct nGraph *, int, int);
int shortestPathLength(struct nGraph *, int, int);
int getEdgeWeight(struct nGraph *, int, int);
int graphRadius(struct nGraph *);
int graphDiameter(struct nGraph *);
struct nGraph getVertices(struct nGraph *);
void makeRandomTree(struct nGraph *, int);
void printPermutations(struct nGraph);
void listAllTrees(struct nGraph *, int);
void addRandomEdge(struct nGraph *, int);
void BronKerbosch(struct nGraph, struct nGraph, struct nGraph, struct nGraph);
void removeVertex(struct nGraph *, int);
void removeVertexPopLast(struct nGraph *);
void listVerticesAlphabet(struct nGraph *);
void listVertices(struct nGraph *);
void listEdges(struct nGraph *);
void copyGraphDuplicateOK(struct nGraph *, struct nGraph *);
void copyGraph(struct nGraph *, struct nGraph *);
void makeCompleteGraph(struct nGraph *, int);
void makeRegularGraph(struct nGraph *, int, int);
int searchVertexCount(struct nGraph *, int);
void setVertexLabel(struct nGraph *, int, char *);
struct nGraph newCompleteGraph(int, char *);
struct nGraph graphCenter(struct nGraph *);
struct nGraph gUnion(struct nGraph *, struct nGraph *);
struct nGraph gIntersection(struct nGraph *, struct nGraph *);
struct nGraph getNeighbours(struct nGraph *, int);
struct nGraph gRingSum(struct nGraph *, struct nGraph *);
struct nGraph crossProduct(struct nGraph *, struct nGraph *);
struct nGraph gUnionVertex(struct nGraph *, int);

// **** Get All Paths From Vertex A to B ***//
int vertexExists(struct nGraph *G, int vertex);
void printPath(struct nGraph *G);
void getAllPaths(int source_vertex, int destination_vertex, struct nGraph *mainGraph, struct nGraph *pathGraph);

// **** Prufer Code and Random Tree Generation Using Prufer Codes ***//
void updateDegrees(struct nGraph *G);
void copyVerticesPrufer(struct prufer *src, struct prufer *dest);
struct prufer getVerticesPrufer(struct prufer *src);
void pruferInit(struct prufer *G, char *t);
struct prufer newPrufer(char *c);
int getPendantVertex(struct nGraph *G);
void addVertexPrufer(struct prufer *P, int c);
struct prufer generatePruferCodeFromTree(struct nGraph *G);
struct nGraph generateTreeFromPruferCode(struct prufer *P, struct nGraph *Vertices);
void removeVertexPrufer(struct prufer *P, int c);
int getRandomNumber(int MaxRange);
struct prufer getRandomPruferCode(int pruferSize, int pruferMaxNumber);
struct nGraph getRandomPruferTree(int pruferSize, int pruferMaxNumber);
void showPrufer(struct prufer *P);
void sortVertices(struct nGraph *G);

#endif
