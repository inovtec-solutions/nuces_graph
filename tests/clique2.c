#include "nucesGraph.h"

int main(int argc, char *argv[])
{
	struct nGraph G = newGraph("G");
	struct nGraph P, R, X;

	int it;
	for (it = 0; it < 4; it++)
		addVertex(&G, it);

	addEdge(&G, 0, 1, 0);
	addEdge(&G, 0, 2, 0);
	addEdge(&G, 1, 3, 0);
	addEdge(&G, 1, 2, 0);
	addEdge(&G, 2, 3, 0);
	addEdge(&G, 0, 3, 0);

	P = getVertices(&G);
	R = newGraph("R");
	X = newGraph("X");

	BronKerbosch(G, P, R, X);

	return 0;
}
