#include "nucesGraph.h"

int main(int argc, char *argv[])
{
	struct nGraph G = newGraph("G");
	makeRegularGraph(&G, 4, 2); // first int is # of vertices
								// second int is degree of each vertice
	show(&G);

	//struct nGraph B = newCompleteGraph(5, "G");
	//show(&B);

	return 0;
}
