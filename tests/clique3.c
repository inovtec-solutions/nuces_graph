#include <stdio.h>
#include "nucesGraph.h"

#define nEdges 5000

int main(int argc, char *argv[])
{
	struct nGraph G = newGraph("G");

	struct nGraph P = getVertices(&G);
	struct nGraph R = newGraph("R");
	struct nGraph X = newGraph("X");

	int maximum, it, i;
	for (it = 0; it <= nEdges; it += 2000) {
		struct nGraph G = newGraph("G");	

		for (i = 0; i < 100; i++) 
			addVertex(&G, i);
		for (i = 0; i < it; i++) 
			addRandomEdge(&G, 0);

		P = getVertices(&G);
		R = newGraph("R");
		X = newGraph("X");
	
		maximum = 0;
	
		BronKerbosch(G, P, R, X);
		
		fprintf(stderr, "%d %d\n", it, maximum);
	}

	return 0;
}

