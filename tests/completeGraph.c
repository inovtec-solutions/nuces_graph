#include "nucesGraph.h"

int main(int argc, char *argv[])
{
	struct nGraph G = newGraph("G");
	makeCompleteGraph(&G, 5);

	show(&G);

	struct nGraph B = newCompleteGraph(5, "G");
	show(&B);

	return 0;
}
