#include <stdio.h>
#include "nucesGraph.h"

int main(int argc, char *argv[])
{
	struct nGraph G = newGraph("G");

	int i;
	for (i = 0; i < 6; i++) {
		addVertex(&G, i);
	}

	addEdge(&G, 0, 1, 2);
	addEdge(&G, 0, 2, 3);
	addEdge(&G, 1, 2, 2);
	addEdge(&G, 1, 3, 1);
	addEdge(&G, 2, 4, 1);
	addEdge(&G, 1, 4, 3);
	addEdge(&G, 3, 4, 2);
	addEdge(&G, 3, 5, 1);
	addEdge(&G, 4, 5, 2);
	addEdge(&G, 1, 5, 2);

	int j;
	for (j = 0; j < 6; j++) {
		for (i = j; i < 6; i++) {
			if (i != j) {
				printf("length: %d -> %d = %d\n", i, j, shortestPathLength(&G, i, j));
			}
		}
	}

	return 0;
}

