/*
 * S2DPaths.cpp
 *
 *  Created on: Nov 2, 2018
 *      Author: baharali
 */

#include <string.h>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "nucesGraph.h"

int main()
{
	/* Create Instance of Graph */
	struct nGraph mainGraph = newGraph("Main Graph");

	/* Add Vertices to Graph */
	addVertex(&mainGraph, 1);
	addVertex(&mainGraph, 2);
	addVertex(&mainGraph, 3);
	addVertex(&mainGraph, 4);
	addVertex(&mainGraph, 5);
	addVertex(&mainGraph, 6);

	/* Add Edges to Graph */
	addEdge(&mainGraph, 1, 2, 0);
	addEdge(&mainGraph, 1, 3, 0);
	addEdge(&mainGraph, 1, 4, 0);
	addEdge(&mainGraph, 1, 5, 0);
	addEdge(&mainGraph, 1, 6, 0);
	addEdge(&mainGraph, 3, 4, 0);
	addEdge(&mainGraph, 3, 5, 0);
	addEdge(&mainGraph, 5, 6, 0);
	addEdge(&mainGraph, 3, 6, 0);

	/* Show Graph */
	show(&mainGraph);

	/* Create Path Graph, Which will save vertices and edges of Path Graph*/
	struct nGraph pathGraph = newGraph("Path Graph");

	/* Declare Source Vertex and Destination Vertex*/
	int source_vertex = 2;
	int destination_vertex = 3;

	/* Get All Paths from Source Vertex to Destination Vertex*/
	getAllPaths(source_vertex, destination_vertex, &mainGraph, &pathGraph);
}

