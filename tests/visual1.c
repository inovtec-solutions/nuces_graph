#include "nucesGraph.h"

int main(int argc, char *argv[])
{
	int i;
	struct nGraph G = newGraph("G");

	for (i = 0; i < 6; i++) {
		addVertex(&G, i);
	}

	addEdge(&G, 0, 1, 2);
	addEdge(&G, 0, 2, 3);
	addEdge(&G, 1, 2, 2);
	addEdge(&G, 1, 3, 1);
	addEdge(&G, 2, 4, 1);
	addEdge(&G, 1, 4, 3);
	addEdge(&G, 3, 4, 2);
	addEdge(&G, 3, 5, 1);
	addEdge(&G, 4, 5, 2);
	addEdge(&G, 1, 5, 2);

	exportDot(&G);
	showDot(&G);

	return 0;
}
