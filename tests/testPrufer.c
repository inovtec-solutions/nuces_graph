/*
 * S2DPaths.cpp
 *
 *  Created on: Nov 2, 2018
 *      Author: baharali
 */

#include <string.h>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "nucesGraph.h"

int main()
{

	printf("\n**** Tree Graph ****\n");
	struct nGraph G = newGraph("G");
	addVertex(&G, 1);
	addVertex(&G, 2);
	addVertex(&G, 3);
	addVertex(&G, 4);
	addVertex(&G, 5);

	addEdge(&G, 1, 2, 0);
	addEdge(&G, 1, 3, 0);
	addEdge(&G, 1, 5, 0);
	addEdge(&G, 4, 5, 0);
	show(&G);

	printf("\n**** Prufer Code From Tree ****\n");
	struct prufer P = generatePruferCodeFromTree(&G);
	showPrufer(&P);

	printf("\n__________________________________________________________\n");
	struct nGraph VerticesGraph = newGraph("G");
	addVertex(&VerticesGraph, 1);
	addVertex(&VerticesGraph, 2);
	addVertex(&VerticesGraph, 3);
	addVertex(&VerticesGraph, 4);
	addVertex(&VerticesGraph, 5);
	show(&VerticesGraph);

	struct prufer P2 = newPrufer("Prufer2");
	addVertexPrufer(&P2, 1);
	addVertexPrufer(&P2, 1);
	addVertexPrufer(&P2, 5);

	printf("\n**** Prufer Code ****\n");
	showPrufer(&P2);

	printf("\n**** Tree From Prufer Code ****\n");
	struct nGraph PG = generateTreeFromPruferCode(&P2, &VerticesGraph);
	show(&PG);

	printf("\n__________________________________________________________\n");
	printf("\n**** Random Tree Using Prufer Code ****\n");
	srand ( time(NULL) );
	struct nGraph RPG = getRandomPruferTree(5,10);
	show(&RPG);

}
