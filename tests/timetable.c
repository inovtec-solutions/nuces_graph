#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "nucesGraph.h"

int main(int argc, char *argv[])
{
	FILE 	*fd = fopen(argv[1], "r");
	FILE 	*ft = fopen(argv[2], "r");
	
	size_t  linesize = 300;
	char 	*line = malloc(sizeof(char)*linesize);
	char 	*index, *code1, *code2, *section, *remain;

	//int indextable[500];
	char code1table[500][20];
	char code2table[500][20];
	char sectiontable[500][20];
	char sectionchoice[20];
	int entrycount = 0;

	struct nGraph G = newGraph("G");
	
	size_t len; 

	/* Getting Data from file and adding to nGraph
	 */
	int looper;

	if (ft) {
		while (fgets(line, linesize, fd) != NULL) 
		{
			printf("%s\n", line);
		}
	}

	exit(0);

	if (fd) {
		while ( fgets(line, linesize, fd) != NULL)
		{	
			index = strtok_r(line, "\t", &remain);
			if ( atoi(index) == 1 && entrycount > 0) {
				for (looper = 0; looper < entrycount; looper++) {
					if (strcmp(code1table[looper], code2table[looper]) == 0) {
						strcpy(sectionchoice, sectiontable[looper]);
						break;
					}
				}
				for (looper = 0; looper < entrycount; looper++) {
					strcat(code1table[looper], "\n");
					strcat(code2table[looper], "\n");
					
					strcat(code1table[looper], sectionchoice);
					strcat(code2table[looper], sectiontable[looper]);
				}
				for (looper = 0; looper < entrycount; looper++) {
					printf("%d %s and %s\n", looper, code1table[looper], code2table[looper]);
					addVertexLabel(&G, code1table[looper]); 
					addVertexLabel(&G, code2table[looper]);
	
					addEdgeLabel(&G, code1table[looper], code2table[looper], 0);	
				}
				entrycount = 0;
				printf("\n\n");
			}		
			
			//indextable[entrycount] = atoi(index);

			code1 = strtok_r(remain, "\t", &remain);
			code2 = strtok_r(remain, "\t", &remain);

			section = strtok_r(remain, "\t", &remain);
			section = strtok_r(remain, "\t", &remain);

			strcpy(code1table[entrycount], code1);
			strcpy(code2table[entrycount], code2);
			strcpy(sectiontable[entrycount], section);

			len = strlen(code1table[entrycount]);
			if (len && ((code1table[entrycount][len-1] == '\n') || (code1table[entrycount][len-1] == '\r'))) {
			    code1table[entrycount][len-1] = '\0';
			}
			len = strlen(code2table[entrycount]);
			if (len && ((code2table[entrycount][len-1] == '\n') || (code2table[entrycount][len-1] == '\r'))) {
			    code2table[entrycount][len-1] = '\0';
			}
			len = strlen(sectiontable[entrycount]);
			if (len && ((sectiontable[entrycount][len-1] == '\n') || (sectiontable[entrycount][len-1] == '\r'))) {
			    sectiontable[entrycount][len-1] = '\0';
			}

			entrycount++;
		}
	}

	fclose(fd);
	if (line) free(line);

	/* Greedy Coloring starts here
	 */
	struct vertex *tmpV1 = G.V->head;
	struct vertex *tmpV2;

	struct nGraph neigh, color;

	int candidate = -1;
	int chromatic = 0;

	while (tmpV1 != NULL) {
		neigh = getNeighbours(&G, tmpV1->label);
		color = newGraph("c");

		/* Prepare Color Exclusion List */
		tmpV2 = neigh.V->head;
		while (tmpV2 != NULL) {
			if (getVertexColor(&G, tmpV2->label) != -1) {
				addVertex(&color, getVertexColor(&G, tmpV2->label));
			}
			tmpV2 = tmpV2->next;
		}
		if (color.V->count >= chromatic)
			chromatic = color.V->count;

		if (color.V->count > 20) 
			printf("too much colors");

		int z;
		for (z = 0; z < 20; z++) {
			if (!searchVertex(&color, z)) { 
				candidate = z;
				break;
			}
		}
		tmpV1->color = candidate;

		tmpV1 = tmpV1->next;
	}

	printf("chromatic number: %d\n", chromatic+1);
 	showDot(&G);

	exit(0);
}
